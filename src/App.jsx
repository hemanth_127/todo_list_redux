import React from 'react'
import './App.css'
import { SnackbarProvider } from 'notistack'
import Home from './Home'

function App () {
  return (
    <>
      <SnackbarProvider>
        <Home />
      </SnackbarProvider>
    </>
  )
}

export default App
