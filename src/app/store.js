import { configureStore } from '@reduxjs/toolkit'
import projectsReducer from '../features/Projects/projectSlice'
import todoReducer from '../features/TODO/todoSlice'
import sectionReducer from '../features/Sections/SectionSlice'
import errorReducer from '../features/Error/errorSlicer'
import labelReducer from '../features/Labels/labelSlicer'

export const store = configureStore({
  reducer: {
    projects: projectsReducer,
    todo: todoReducer,
    section: sectionReducer,
    error: errorReducer,
    label: labelReducer
  }
})

export default store
