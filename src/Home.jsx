import React from 'react'
import ProjectView from './features/Projects/ProjectView'
import TodoView from './features/TODO/TodoView'
import { Routes, Route } from 'react-router-dom'
import ErrorPage from './features/Error/ErrorPage'
import { useSelector } from 'react-redux'
import NoMatch from './features/Error/NoMatch'
import LabelView from './features/Labels/LabelView'

const Home = () => {
  const { error } = useSelector(state => state.error)

  return (
    <div>
      {error ? (
        <div className='error'>
          <ErrorPage error={error} />
        </div>
      ) : (
        <div className='container'>
          <div className='leftSide'>
            
            <ProjectView />
          </div>
          <div className='rightSide'>
            <Routes>
              <Route path='/' element={<TodoView />} />
              <Route path='app/project/:id' element={<TodoView />} />
              <Route path='app/label' element={<LabelView />} />
              <Route path='*' element={<NoMatch />} />
            </Routes>
          </div>
        </div>
      )}
    </div>
  )
}

export default Home
