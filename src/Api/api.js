import { TodoistApi } from '@doist/todoist-api-typescript'
import { v4 as uuidv4 } from 'uuid'
import axios from 'axios'

const token = '0527497462fcdcf7b2191d06e2303f9d39ee8bd9'
const api = new TodoistApi(token)

export const getProject = async () => {
  const response = await api.getProjects()
  return response
}
export default getProject

export const addProject = async ({ name, color, favourite }) => {
  const response = await api.addProject({
    name: `${name}`,
    isFavorite: favourite,
    color
  })
  return response
}

export const deleteProject = async id => {
  const response = await api.deleteProject(`${id}`)
  return response
}

export const updateProject = async ({ id, name, favourite, color }) => {
  if (name) {
    return await api.updateProject(`${id}`, {
      name: `${name}`,
      isFavorite: `${favourite}`,
      color
    })
  } else {
    return await api.updateProject(`${id}`, {
      is_favorite: `${favourite}`
    })
  }
}

//---------------------------------------------------------------------------
// Tasks

export const reopenTask = async id => {
  return api.reopenTask(id)
}

export const updateTask = async (id, updateData) => {
  const { name, description, selectedDate, dueString, labelName, selectedId ,sec} =
    updateData

  let response
  if (selectedId) {
    const uuid = uuidv4()
    const data = {
      commands: [
        {
          type: 'item_move',
          uuid: uuid,
          args: {
            id: id,
            project_id: selectedId
          }
        }
      ]
    }

    const headers = {
      Authorization: `Bearer ${token}`
    }

    const moveResponse = await axios.post(
      'https://api.todoist.com/sync/v9/sync',
      data,
      {
        headers
      }
    )
    const res = await api.updateTask(id, {
      content: `${name}`,
      description: `${description}`,
      due_date: `${selectedDate}`,
      due_string: `${dueString}`,
      labels: [`${labelName}`]
    })

    response = res
  } else {
    response = await api.updateTask(id, {
      content: `${name}`,
      description: `${description}`,
      due_date: `${selectedDate}`,
      due_string: `${dueString}`,
      labels: [`${labelName}`]
    })
  }
  return response
}

export const getTasks = async () => {
  const response = await api.getTasks()
  return response
}

export const addTask = async ({
  name,
  projectId,
  todaydate,
  description,
  selectedDate,
  dueString,
  labelName
}) => {
  let response
  if (projectId) {
    response = await api.addTask({
      content: `${name}`,
      projectId: `${projectId}`,
      description: `${description}`,
      due_date: `${selectedDate}`,
      due_string: `${dueString}`,
      labels: [`${labelName}`]
    })
  } else {
    response = await api.addTask({
      content: `${name}`,
      due_date: `${selectedDate || todaydate}`,
      description: `${description}`,
      due_string: 'Today',
      labels: [`${labelName}`]
    })
  }
  return response
}

export const addSectionTask = async ({
  name,
  projectId,
  sectionId,
  description,
  selectedDate,
  dueString,
  labelName
}) => {
  let response
  if (projectId || sectionId) {
    response = await api.addTask({
      content: `${name}`,
      projectId: `${projectId}`,
      sectionId: `${sectionId}`,
      description: `${description}`,
      due_date: `${selectedDate}`,
      due_string: `${dueString}`,
      labels: [`${labelName}`]
    })
  }
  return response
}

export const deleteTask = async id => {
  const response = await api.deleteTask(id)
  return response
}

export const completeTask = async id => {
  return api.closeTask(id)
}

// -------------------------------------------
// Section

export const updateSection = async (id, name) => {
  return api.updateSection(id, { name })
}

export const getSections = async id => {
  const response = await api.getSections(id)
  return response
}

export const addSection = async ({ name, projectId }) => {
  const response = await api.addSection({
    name: `${name}`,
    projectId: `${projectId}`
  })
  return response
}

export const deleteSection = async id => {
  const response = await api.deleteSection(id)
  return response
}

// ----------------------------------------
//Labels

export const getLabels = async () => {
  const response = await api.getLabels()
  return response
}

export const addLabel = async ({ name }) => {
  const response = await api.addLabel({ name: `${name}` })
  return response
}

export const deleteLabel = async labelId => {
  const response = await api.deleteLabel(labelId)
  return response
}

export const updateLabel = async ({ labelId, name }) => {
  const response = await api.updateLabel(labelId, { name: `${name}` })
  return response
}
