// import React from 'react'
// import { SettingOutlined,DeleteOutlined } from '@ant-design/icons'
// import { Menu } from 'antd'
// const data = [
//   {
//     id: '220474322',
//     parentId: null,
//     order: 0,
//     color: 'grey',
//     name: 'Inbox',
//     commentCount: 10,
//     isShared: false,
//     isFavorite: false,
//     isInboxProject: true,
//     isTeamInbox: false,
//     url: 'https://todoist.com/showProject?id=220474322',
//     viewStyle: 'list'
//   },
//   {
//     id: '123456789',
//     parentId: '220474322',
//     order: 1,
//     color: 'blue',
//     name: 'Work',
//     commentCount: 5,
//     isShared: true,
//     isFavorite: true,
//     isInboxProject: false,
//     isTeamInbox: false,
//     url: 'https://todoist.com/showProject?id=123456789',
//     viewStyle: 'board'
//   },
//   {
//     id: '987654321',
//     parentId: '220474322',
//     order: 2,
//     color: 'green',
//     name: 'Personal',
//     commentCount: 3,
//     isShared: false,
//     isFavorite: true,
//     isInboxProject: false,
//     isTeamInbox: false,
//     url: 'https://todoist.com/showProject?id=987654321',
//     viewStyle: 'list'
//   },
//   {
//     id: '987654329',
//     parentId: '220474322',
//     order: 2,
//     color: 'green',
//     name: 'cobra',
//     commentCount: 3,
//     isShared: false,
//     isFavorite: true,
//     isInboxProject: false,
//     isTeamInbox: false,
//     url: 'https://todoist.com/showProject?id=987654321',
//     viewStyle: 'list'
//   }
// ]

// function getItem (label, key, icon, children, type) {
//   return {
//     key,
//     children,
//     label,
//     icon,
//     type
//   }
// }

// const items = [
//   getItem(
//     'Navigation Three',
//     'sub4',
//     <SettingOutlined />,
//     data.map(ele => getItem(ele.name, ele.id, <DeleteOutlined />))
//   )
// ]
// const Rough = () => {
//   const onClick = e => {
//     console.log('click ', e)
//   }
//   return (
//     <Menu
//       onClick={onClick}
//       style={{
//         width: 256
//       }}
//       defaultSelectedKeys={['1']}
//       defaultOpenKeys={['sub1']}
//       mode='inline'
//       items={items}
//     />
//   )
// }
// export default Rough
import React from 'react'
import { DownOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Dropdown, Space } from 'antd'

const handleMenuClick = selectedItem => {
  console.log(selectedItem.color)
}

const items = [
  { key: '1', label: 'Berry Red', color: 'lime_green', icon: <UserOutlined /> },
  { key: '2', label: 'Red', color: 'red', icon: <UserOutlined /> },
  { key: '3', label: 'Orange', color: 'orange' },
  { key: '4', label: 'Yellow', color: 'yellow' },
  { key: '5', label: 'Olive Green', color: 'olive_green' },
  { key: '6', label: 'Lime Green', color: 'lime_green' },
  { key: '7', label: 'Green', color: 'green' },
  { key: '8', label: 'Mint Green', color: 'mint_green' },
  { key: '9', label: 'Teal', color: 'teal' },
  { key: '10', label: 'Sky Blue', color: 'sky_blue' },
  { key: '11', label: 'Light Blue', color: 'light_blue' },
  { key: '12', label: 'Blue', color: 'blue' },
  { key: '13', label: 'Grape', color: 'grape' },
  { key: '14', label: 'Violet', color: 'violet' },
  { key: '15', label: 'Lavender', color: 'lavender' },
  { key: '16', label: 'Magenta', color: 'magenta' },
  { key: '17', label: 'Salmon', color: 'salmon' },
  { key: '18', label: 'Charcoal', color: 'charcoal' },
  { key: '19', label: 'Grey', color: 'grey' },
  { key: '20', label: 'Taupe', color: 'taupe' }
  //   {
  //     label: '1st menu ',
  //     key: '1',
  //     icon: <UserOutlined />
  //   }
]
const menuProps = {
  items,
  onClick: ({ key }) => {
    const selectedItem = items.find(item => item.key === key)
    handleMenuClick(selectedItem)
  }
}
const CustomDropdown = () => (
  <Space wrap>
    <Dropdown menu={menuProps} trigger={['click']}>
      <Button>
        <Space>
          Button
          <DownOutlined />
        </Space>
      </Button>
    </Dropdown>
  </Space>
)
export default CustomDropdown
