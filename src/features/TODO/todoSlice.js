import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  todoData: [],
  loading: false
}

const todoSlice = createSlice({
  name: 'todo',
  initialState,
  reducers: {
    setTodoData: (state, action) => {
      state.todoData = action.payload
    },
    setLoading: (state, action) => {
      state.loading = action.payload
    },
    addTaskTodo: (state, action) => {
      state.todoData.push(action.payload)
    },
    deleteTaskTodo: (state, action) => {
      state.todoData = state.todoData.filter(ele => ele.id !== action.payload)
    },
    checkboxTaskTodo: (state, action) => {
      state.todoData = state.todoData.map(ele => {
        if (ele.id === action.payload) {
          return { ...ele, isCompleted: true }
        }
        return ele
      })
    },
    updateTaskTodo: (state, action) => {
      state.todoData = state.todoData.map(ele => {
        if (ele.id === action.payload.id) {
          return action.payload
        }
        return ele
      })
    },
    reOpenTodo: (state, action) => {
      state.todoData = state.todoData.map(ele => {
        if (ele.id === action.payload) {
          return { ...ele, isCompleted: false }
        }
        return ele
      })
    }
  }
})

export default todoSlice.reducer
export const {
  setTodoData,
  setLoading,
  setTaskProjectData,
  addTaskTodo,
  deleteTaskTodo,
  checkboxTaskTodo,
  updateTaskTodo,
  reOpenTodo
} = todoSlice.actions
