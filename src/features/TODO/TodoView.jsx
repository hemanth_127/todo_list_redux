import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import {
  getTasks,
  addTask,
  completeTask,
  deleteTask,
  updateTask,
  reopenTask
} from '../../Api/api'

import {
  setTodoData,
  setLoading,
  addTaskTodo,
  checkboxTaskTodo,
  deleteTaskTodo,
  updateTaskTodo,
  reOpenTodo
} from './todoSlice'

import { useDispatch, useSelector } from 'react-redux'
import { Popover, Spin } from 'antd'
import { Button, Checkbox } from 'antd'
import {
  PlusOutlined,
  CalendarOutlined,
  TagOutlined,
  EditOutlined,
  DeleteOutlined,
  EllipsisOutlined
} from '@ant-design/icons'
import FormPage from '../Form/FormPage'
import SectionView from '../Sections/SectionView'
import { useSnackbar } from 'notistack'
import { setError } from '../Error/errorSlicer'

const TodoView = () => {
  const [isModalOpen, setIsModalOpen] = useState(false)
  const { todoData, loading } = useSelector(state => state.todo)
  const [editData, setEditData] = useState(null)

  const { enqueueSnackbar } = useSnackbar()
  const dispatch = useDispatch()
  const prop = useParams().id
  let [id, projectName] = prop ? prop.split('-') : ''

  const showModal = () => {
    setIsModalOpen(true)
  }

  const handleCancel = () => {
    setIsModalOpen(false)
    setEditData(null)
  }

  const getTasksData = async () => {
    dispatch(setLoading(true))
    try {
      const data = await getTasks()
      dispatch(setTodoData(data))
    } catch (error) {
      dispatch(setError(error.message))
    } finally {
      dispatch(setLoading(false))
    }
  }

  useEffect(() => {
    getTasksData()
  }, [])

  const handleAddtask = async ({
    name,
    description,
    selectedDate,
    dueString,
    selectedId,
    labelName
  }) => {
    try {
      const projectId = selectedId || id || ''
      const data = await addTask({
        name,
        projectId: `${projectId}`,
        todaydate,
        description,
        selectedDate,
        dueString,
        labelName
      })
      dispatch(addTaskTodo(data))
      enqueueSnackbar('Added Task', { autoHideDuration: 1000 })
      setIsModalOpen(false)
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleChecbox = async taskId => {
    try {
      const res = await completeTask(taskId)
      dispatch(checkboxTaskTodo(taskId))
      enqueueSnackbar('Task Completed', {
        action: <Button onClick={() => handleReOpen(taskId)}>undo</Button>,
        autoHideDuration: 1000
      })
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleReOpen = async id => {
    try {
      const data = await reopenTask(id)
      dispatch(reOpenTodo(id))
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleDeleteTask = async taskId => {
    try {
      const res = await deleteTask(taskId)
      dispatch(deleteTaskTodo(taskId))
      enqueueSnackbar('Task Deleted', { autoHideDuration: 1000 })
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleSetEditData = taskId => {
    const data = todoData.filter(ele => ele.id === taskId)
    const updateData = data[0]
    setEditData(updateData)
  }

  const handleUpdateTask = async ({
    name,
    description,
    selectedDate,
    dueString,
    selectedId,
    labelName
  }) => {
    try {
      const res = await updateTask(editData?.id, {
        selectedId,
        name,
        description,
        selectedDate,
        dueString,
        labelName
      })

      dispatch(updateTaskTodo(res))
      setEditData(null)
      setIsModalOpen(false)
      enqueueSnackbar('Task Updated')
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const today = new Date()
  const year = today.getFullYear()
  const month = today.getMonth() + 1
  const day = today.getDate()
  let todaydate = `${year}-${month < 10 ? '0' + month : month}-${
    day < 10 ? '0' + day : day
  }`

  let todolist
  if (id) {
    todolist = todoData
      .filter(ele => ele.projectId === id && !ele.sectionId)
      .filter(l => l.isCompleted === false)
  } else {
    todolist = todoData
      .filter(ele => ele?.due?.date === todaydate)
      .filter(l => l.isCompleted === false)
  }

  return (
    <>
      <div className='todo_view'>
        {id && <p style={{ fontWeight: 'bold' }}>My Project / </p>}
        <div className='conent_View'>
          <div className='todoList'>
            {!id && <h2>Today</h2>}
            <h3>{projectName}</h3>

            {loading && <Spin className='spin' />}

            {todolist &&
              todolist.map(ele => {
                return (
                  <div key={ele.id}>
                    {editData?.id === ele.id ? (
                      <FormPage
                        handleAdd={editData ? handleUpdateTask : handleAddtask}
                        handleCancel={handleCancel}
                        isModalOpen={isModalOpen}
                        editData={editData}
                      />
                    ) : (
                      <div className='task-item'>
                        <div className='todoview-container'>
                          <div>
                            <Checkbox
                              className='checkbox-item'
                              onChange={() => {
                                handleChecbox(ele.id)
                              }}
                            />
                            {ele.content}
                          </div>

                          <div className='task-hover'>
                            <Button
                              type='text'
                              onClick={e => {
                                e.stopPropagation()
                                handleSetEditData(ele.id)
                                showModal()
                              }}
                            >
                              <EditOutlined />
                            </Button>

                            <Popover
                              arrow={false}
                              content={
                                <div className='popover-item'>
                                  <Button
                                    type='text'
                                    style={{
                                      float: 'right',
                                      color: 'red'
                                    }}
                                    onClick={e => {
                                      e.stopPropagation()
                                      handleDeleteTask(ele.id)
                                    }}
                                  >
                                    <DeleteOutlined /> Delete
                                  </Button>
                                </div>
                              }
                              trigger='click'
                              placement='bottom'
                            >
                              <Button
                                type='text'
                                style={{
                                  float: 'right',
                                  margin: '0 6px'
                                }}
                                onClick={e => e.stopPropagation()}
                              >
                                <span className='ellipsisicon'>
                                  <EllipsisOutlined />
                                </span>
                              </Button>
                            </Popover>
                          </div>
                        </div>
                        <div className='details'>
                          {ele.description && <p> {ele.description}</p>}

                          <div
                            style={{
                              display: 'flex',
                              paddingTop: '.4rem'
                            }}
                          >
                            <p>
                              {id && ele.due?.string && (
                                <div className='desription'>
                                  <CalendarOutlined />
                                  <span>
                                    {ele.due.date === todaydate ? (
                                      <span className='today'>{'Today'}</span>
                                    ) : (
                                      ele.due.date.slice(-2) +
                                      ele.due.string.slice(7, 11)
                                    )}
                                  </span>
                                </div>
                              )}
                            </p>
                            <p>
                              {ele?.labels?.[0] && (
                                <div className='desription'>
                                  <TagOutlined />
                                  <span>{ele.labels[0]}</span>
                                </div>
                              )}
                            </p>
                          </div>
                        </div>
                      </div>
                    )}
                    <hr className='row' />
                  </div>
                )
              })}
          </div>

          <div>
            {!isModalOpen && (
              <Button
                className='plus-btn'
                type='text'
                icon={<PlusOutlined className='plushover' />}
                onClick={e => {
                  e.stopPropagation()
                  showModal()
                }}
              >
                <span className='text-color'>Add Task</span>
              </Button>
            )}
            {!editData && (
              <div className='formpage'>
                <FormPage
                  handleAdd={editData ? handleUpdateTask : handleAddtask}
                  handleCancel={handleCancel}
                  isModalOpen={isModalOpen}
                  editData={editData}
                />
              </div>
            )}
          </div>

          <div className='Sectionlist'>
            {id && <SectionView projectId={id} />}
          </div>
        </div>
      </div>
    </>
  )
}

export default TodoView
