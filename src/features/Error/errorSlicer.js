import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  error: null
}

const errorSlicer = createSlice({
  name: 'error',
  initialState,
  reducers: {
    setError: (state, action) => {
      state.error = action.payload
    }
  }
})

export default errorSlicer.reducer
export const { setError } = errorSlicer.actions
