import React, { useState, useEffect } from 'react'
import { setError } from '../Error/errorSlicer'
import { useDispatch } from 'react-redux'
import { getLabels } from '../../Api/api'

function LabelsDropdown ({ setLabelName, labelName }) {
  const [labelData, setLabelData] = useState([])
  const dispatch = useDispatch()

  const fetchLabelData = async () => {
    try {
      const data = await getLabels()
      setLabelData(data)
    } catch (error) {
      dispatch(setError(error.message))
    }
  }
  useEffect(() => {
    fetchLabelData()
  }, [])

  const handleSelect = e => {
    setLabelName(e.target.value)
  }

  return (
    <div className='label-data'>
      <select onChange={handleSelect} value={labelName} className='selector'>
        {labelData.map(ele => {
          return (
            <option key={ele.id} value={`${ele.name}`}>
              {ele.name}
            </option>
          )
        })}
      </select>
    </div>
  )
}

export default LabelsDropdown
