import React from 'react'
import { useSelector } from 'react-redux'

function ProjectsTitles ({ setSelectedId }) {
  const { projectData } = useSelector(state => state.projects)

  const handleSelect = e => {
    setSelectedId(e.target.value)
  }
  return (
    <div className='project-picker'>
      <select onClick={handleSelect} className='selector'>
        {projectData.map(ele => {
          return (
            <option key={ele.id} value={`${ele.id}`}>
              {ele.name}
            </option>
          )
        })}
      </select>
    </div>
  )
}

export default ProjectsTitles
