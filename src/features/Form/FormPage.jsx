import { Modal, Input, Button } from 'antd'
import React, { useEffect, useState } from 'react'
const { TextArea } = Input
import { DatePicker, Switch } from 'antd'
import ColorDropdown from './ColorDropdown'
import ProjectsTitles from './ProjectsTitles'
import LabelsDropdown from './LabelsDropdown'

const FormPage = ({
  handleAdd,
  handleCancel,
  isModalOpen,
  title = `Add Task`,
  editData
}) => {
  const [name, setName] = useState('')
  const [description, setDescript] = useState('')
  const [selectedDate, setSelectedDate] = useState('')
  const [color, setColor] = useState('charcoal')
  const [dueString, setDueString] = useState('')
  const [favourite, setFavourite] = useState(false)
  const [selectedId, setSelectedId] = useState('')
  const [labelName, setLabelName] = useState('')

  useEffect(() => {
    if (editData) {
      if (editData?.content) {
        setName(editData.content)
      } else if (editData?.name) {
        setName(editData.name)
      } else {
        setName('')
      }
      if (editData?.description) {
        setDescript(editData.description)
      } else {
        setDescript('')
      }
      if (editData?.isFavorite) {
        setFavourite(editData.isFavorite)
      } else {
        setFavourite(false)
      }

      if (editData?.due?.date) {
        setSelectedDate(editData.due.date)
      } else {
        setSelectedDate('')
      }
      if (editData?.due?.string) {
        setDueString(editData.due.string)
      } else {
        setDueString(``)
      }
      if (editData?.labels?.[0]) {
        setLabelName(editData.labels[0])
      } else {
        setLabelName('')
      }

      if (editData?.color) {
        setColor(editData.color)
      } else {
        setColor('charcoal')
      }
    }
  }, [editData])

  const handleSubmit = e => {
    e.preventDefault()
    handleAdd({
      name,
      description,
      selectedDate,
      dueString,
      color,
      favourite,
      selectedId,
      labelName
    })
    setName('')
    setDueString('')
    setDescript('')
    setSelectedDate('')
    setFavourite(false)
    setSelectedId('')
    setLabelName('')
  }

  const cancel = () => {
    handleCancel()
    setName('')
    setDueString('')
    setDescript('')
    setSelectedDate('')
    setFavourite(false)
    setSelectedId('')
    setLabelName('')
  }

  const onDateChange = (date, dateString) => {   
    setSelectedDate(dateString)
    setDueString(date)
  }
  const handelColor = selectedColor => {
    setColor(selectedColor)
  }
  const onFavouiteChange = checked => {
    setFavourite(checked)
  }

  const handleSelectedId = id => {
    setSelectedId(id)
  }

  const handleSelectedLabel = selectedLabel => {
    setLabelName(selectedLabel)
  }

  return (
    <>
      {(title === `Project` || title === `Label`) && (
        <Modal
          title={`Add`+title}
          open={isModalOpen}
          onOk={handleSubmit}
          onCancel={cancel}
          footer={[
            <Button key='cancel' onClick={handleCancel}>
              Cancel
            </Button>,
            <Button key='add' type='primary' onClick={handleSubmit}>
              Add
            </Button>
          ]}
        >
          <form onSubmit={handleSubmit}>
            <Input
              required
              placeholder='Name'
              value={name}
              onChange={e => setName(e.target.value)}
            />

            {title === `Project` && (
              <>
                <ColorDropdown setColor={handelColor} color={color} />
                <Switch value={favourite} onChange={onFavouiteChange} />
                <span style={{ fontWeight: 'bold' }}> Add to Favourite</span>
              </>
            )}

            <button type='submit' style={{ display: 'none' }} />
          </form>
        </Modal>
      )}

      {isModalOpen && (title === `Add Task` || title === `Add section`) && (
        <div className='form-page'>
          <form onSubmit={handleSubmit}>
            <Input
              className='border-input'
              required
              placeholder='Name'
              value={name}
              onChange={e => setName(e.target.value)}
            />
            {title === `Add Task` && (
              <>
                <TextArea
                  className='border-input'
                  rows={1}
                  style={{ marginTop: '1rem' }}
                  placeholder='Desription'
                  value={description}
                  onChange={e => setDescript(e.target.value)}
                />
                <div className='pickers'>
                  <DatePicker onChange={onDateChange} />

                  <LabelsDropdown
                    setLabelName={handleSelectedLabel}
                    labelName={labelName}
                  />
                </div>
                <hr />
                <div>
                  <div className='form-btns'>
                    <ProjectsTitles setSelectedId={handleSelectedId} />
                    <div>
                      <Button
                        key='cancel'
                        className='buttons'
                        onClick={handleCancel}
                      >
                        Cancel
                      </Button>

                      <Button key='add' type='primary' onClick={handleSubmit}>
                        Add Task
                      </Button>
                    </div>
                  </div>
                </div>
              </>
            )}
            {title === `Add section` && (
              <div>
                <Button
                  key='add'
                  className='buttons btn1'
                  type='primary'
                  onClick={handleSubmit}
                >
                  Add Section
                </Button>

                <Button key='cancel' onClick={handleCancel}>
                  Cancel
                </Button>
              </div>
            )}
            <button type='submit' style={{ display: 'none' }} />
          </form>
        </div>
      )}
    </>
  )
}

export default FormPage
