import React from 'react'

const ColorDropdown = ({ setColor, color }) => {
  const handleSelect = e => {
    setColor(e.target.value)
  }

  const colorOptions = [
    { label: 'Berry Red', value: 'berry_red' },
    { label: 'Red', value: 'red' },
    { label: 'Orange', value: 'orange' },
    { label: 'Yellow', value: 'yellow' },
    { label: 'Olive Green', value: 'olive_green' },
    { label: 'Lime Green', value: 'lime_green' },
    { label: 'Green', value: 'green' },
    { label: 'Mint Green', value: 'mint_green' },
    { label: 'Teal', value: 'teal' },
    { label: 'Sky Blue', value: 'sky_blue' },
    { label: 'Light Blue', value: 'light_blue' },
    { label: 'Blue', value: 'blue' },
    { label: 'Grape', value: 'grape' },
    { label: 'Violet', value: 'violet' },
    { label: 'Lavender', value: 'lavender' },
    { label: 'Magenta', value: 'magenta' },
    { label: 'Salmon', value: 'salmon' },
    { label: 'Charcoal', value: 'charcoal' },
    { label: 'Grey', value: 'grey' },
    { label: 'Taupe', value: 'taupe' }
  ]

  return (
    <div className='color-dropdown'>
      <h3>color</h3>
      <select onChange={handleSelect} value={color} className='selector'>
        {colorOptions.map(option => (
          <option key={option.value} value={option.value}>
             {option.label}
          </option>
        ))}
      </select>
    </div>
  )
}

export default ColorDropdown
