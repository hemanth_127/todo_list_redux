import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  projectData: [],
  loading: false
}

const projectSlice = createSlice({
  name: 'projects',
  initialState,
  reducers: {
    setProjectsData: (state, action) => {
      state.projectData = action.payload
    },
    addProjectData: (state, action) => {
      state.projectData.push(action.payload)
    },
    deleteProjectData: (state, action) => {
      state.projectData = state.projectData.filter(
        ele => ele.id !== action.payload
      )
    },
    updateProjectData: (state, action) => {
      state.projectData = state.projectData.map(ele => {
        if (ele.id === action.payload.id) {
          return action.payload
        }
        return ele
      })
    },
    addFavourite: (state, action) => {
      state.projectData = state.projectData.map(ele => {
        if (ele.id === action.payload.id) {
          return action.payload
        } else {
          return ele
        }
      })
    },
    setLoading: (state, action) => {
      state.loading = action.payload
    }
  }
})

export default projectSlice.reducer
export const {
  setProjectsData,
  addProjectData,
  deleteProjectData,
  updateProjectData,
  setLoading,
  addFavourite
} = projectSlice.actions
