import { useDispatch, useSelector } from 'react-redux'
import {
  addProjectData,
  deleteProjectData,
  updateProjectData,
  addFavourite
} from './projectSlice'
import React, { useState } from 'react'
import { Button, Menu } from 'antd'
import {
  BorderlessTableOutlined,
  HeartOutlined,
  EditOutlined,
  EllipsisOutlined,
  DeleteOutlined
} from '@ant-design/icons'
import { Spin } from 'antd'
import { addProject, deleteProject, updateProject } from '../../Api/api'
import { NavLink } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'
import FormPage from '../Form/FormPage'
import { useSnackbar } from 'notistack'
import { setError } from '../Error/errorSlicer'
import { Popover } from 'antd'

const Favourite = () => {
  const [editData, setEditData] = useState(null)
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [open, setOpen] = useState(null)
  const { enqueueSnackbar } = useSnackbar()
  const { projectData, loading } = useSelector(state => state.projects)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const showModal = () => {
    setIsModalOpen(true)
  }
  const handleCancel = () => {
    setIsModalOpen(false)
  }

  const handleHome = () => {
    navigate('/')
  }

  const handleOpenChange = newOpen => {
    setOpen(newOpen)
  }

  const handleAddProject = async ({ name, color, favourite }) => {
    try {
      const data = await addProject({ name, color, favourite })
      dispatch(addProjectData(data))
      setIsModalOpen(false)
      enqueueSnackbar('Added Project')
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleDelete = async id => {
    try {
      const data = await deleteProject(id)
      dispatch(deleteProjectData(id))
      handleHome()
      enqueueSnackbar('Project Deleted')
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleSetEditData = id => {
    const data = projectData.filter(ele => ele.id === id)
    const updateData = data[0]
    setEditData(updateData)
    setIsModalOpen(true)
    setOpen(null)
  }

  const handleEditProject = async ({ name, favourite, color }) => {
    const id = editData.id
    try {
      const data = await updateProject({ id, name, favourite, color })
      dispatch(updateProjectData(data))
      setIsModalOpen(false)
      enqueueSnackbar('Project data Updated')
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleAddFavourite = async id => {
    const favourite = true
    try {
      const data = await updateProject({ id, favourite })
      dispatch(addFavourite(data))
      setOpen(null)
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const favouriteSetter = (id, favourite) => {
    if (favourite) {
      handleUnFavourite(id)
    } else {
      handleAddFavourite(id)
    }
  }

  const handleUnFavourite = async id => {
    const favourite = false
    try {
      const data = await updateProject({ id, favourite })
      dispatch(addFavourite(data))
      setOpen(null)
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  return (
    <div>
      <FormPage
        handleAdd={editData ? handleEditProject : handleAddProject}
        handleCancel={handleCancel}
        isModalOpen={isModalOpen}
        title={`Project`}
        editData={editData}
      />
      <div className='favourite'>
        <Menu mode='inline' className='projects-menu'>
          <Menu.SubMenu
            key='sub1'
            title={
              <span className='projects-sub-menu'>
                Favourites
                <span
                  style={{ marginRight: '2rem' }}
                  onClick={e => {
                    e.stopPropagation()
                    showModal()
                  }}
                ></span>
              </span>
            }
          >
            {loading && <Spin className='spin' />}

            {projectData &&
              projectData.map(ele => {
                if (ele.isFavorite === true) {
                  return (
                    <div key={ele.id}>
                      <Menu.Item>
                        <span style={{ color: `${ele.color}` }}>
                          <BorderlessTableOutlined />
                        </span>
                        <NavLink
                          to={`/app/project/${ele.id}-${ele.name}`}
                          style={{
                            textDecoration: 'none',
                            fontWeight: 'lighter'
                          }}
                          onClick={e => {
                            e.stopPropagation()
                          }}
                        >
                          <span> {ele.name} </span>
                        </NavLink>

                        <Popover
                          content={
                            <div className='popover-item'>
                              <Button
                                onClick={e => {
                                  e.stopPropagation()
                                  handleSetEditData(ele.id)
                                }}
                              >
                                <EditOutlined /> Edit
                              </Button>
                              <Button
                                onClick={e => {
                                  e.stopPropagation()
                                  favouriteSetter(ele.id, ele.isFavorite)
                                }}
                              >
                                {ele.isFavorite === false ? (
                                  <>
                                    <HeartOutlined />
                                    Add to favourites
                                  </>
                                ) : (
                                  <>remove from favourites</>
                                )}
                              </Button>

                              <Button
                                onClick={e => {
                                  e.stopPropagation()
                                  handleDelete(ele.id)
                                }}
                              >
                                <DeleteOutlined />
                                Delete
                              </Button>
                            </div>
                          }
                          placement='right'
                          trigger='click'
                          open={open === ele.id}
                          onOpenChange={handleOpenChange}
                        >
                          <Button
                            type='text'
                            style={{
                              float: 'right',
                              marginTop: '3px'
                            }}
                            onClick={e => {
                              e.stopPropagation()
                              setOpen(ele.id)
                            }}
                          >
                            <EllipsisOutlined />
                          </Button>
                        </Popover>
                      </Menu.Item>
                    </div>
                  )
                }
              })}
          </Menu.SubMenu>
        </Menu>
      </div>
    </div>
  )
}

export default Favourite
