import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  setLabelData,
  setLoading,
  addLabelData,
  deleteLabelData,
  updateLabelData
} from './labelSlicer'
import { getLabels, addLabel, updateLabel, deleteLabel } from '../../Api/api'
import { Button, Popover } from 'antd'
import {
  PlusOutlined,
  EllipsisOutlined,
  DeleteOutlined,
  TagOutlined,
  EditOutlined
} from '@ant-design/icons'
import { useSnackbar } from 'notistack'
import FormPage from '../Form/FormPage'
import { setError } from '../Error/errorSlicer'

import { Spin } from 'antd'

const LabelView = () => {
  const { labelData, loading } = useSelector(state => state.label)
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [editData, setEditData] = useState(null)

  const { enqueueSnackbar } = useSnackbar()
  const dispatch = useDispatch()

  const showModal = () => {
    setIsModalOpen(true)
  }

  const handleCancel = () => {
    setIsModalOpen(false)
  }

  const fetchLabelData = async () => {
    setLoading(true)
    try {
      const data = await getLabels()
      dispatch(setLabelData(data))
    } catch (error) {
      dispatch(setError(error.message))
    } finally {
      dispatch(setLoading(false))
    }
  }
  React.useEffect(() => {
    fetchLabelData()
  }, [])

  const handleAddLabel = async ({ name }) => {
    const data = await addLabel({ name })
    dispatch(addLabelData(data))
    handleCancel()
    enqueueSnackbar('Added label')
  }

  const handleDeleteLabel = async labelId => {
    const data = await deleteLabel(labelId)
    dispatch(deleteLabelData(labelId))
    enqueueSnackbar('Deleted label')
  }

  const handleSetEditData = labelId => {
    const data = labelData.filter(ele => ele.id === labelId)
    const updateData = data[0]
    setEditData(updateData)
  }

  const handleUpdateLabel = async ({ name }) => {
    const labelId = editData.id
    const data = await updateLabel({ labelId, name })
    dispatch(updateLabelData(data))
    enqueueSnackbar('Updated label')
    handleCancel()
  }

  return (
    <div className='todo_view'>
      <div className='label'>
        <h2>Labels</h2>
        <div>
          {!isModalOpen && (
            <Button
              style={{
                marginTop: '.2rem',
                alignSelf: 'center',
                height: '35px',
                color: 'inherit'
              }}
              icon={<PlusOutlined />}
              onClick={e => {
                e.stopPropagation()
                showModal()
              }}
            >
              Add Label
            </Button>
          )}
        </div>
      </div>

      <div className='conent_View'>
        {loading && <Spin className='spin' />}
        {labelData &&
          labelData.map(ele => {
            return (
              <div key={ele.id} className='task-item'>
                <div className='label-item'>
                  <div>
                    <TagOutlined /> {ele.name}
                  </div>

                  <div className='task-hover'>
                    <Button
                      type='text'
                      onClick={e => {
                        e.stopPropagation()
                        handleSetEditData(ele.id)
                        showModal()
                      }}
                    >
                      <EditOutlined />
                    </Button>

                    <Popover
                      arrow={false}
                      content={
                        <div className='popover-item'>
                          <Button
                            type='text'
                            style={{
                              float: 'right',
                              color: 'red'
                            }}
                            onClick={e => {
                              e.stopPropagation()
                              handleDeleteLabel(ele.id)
                            }}
                          >
                            <DeleteOutlined /> Delete
                          </Button>
                        </div>
                      }
                      trigger='click'
                      placement='bottom'
                    >
                      <Button
                        type='text'
                        style={{
                          float: 'right',
                          margin: '0 6px'
                        }}
                        onClick={e => e.stopPropagation()}
                      >
                        <span className='ellipsisicon'>
                          <EllipsisOutlined />
                        </span>
                      </Button>
                    </Popover>
                  </div>
                </div>
              </div>
            )
          })}
      </div>
      <div className='formpage'>
        <FormPage
          handleAdd={editData ? handleUpdateLabel : handleAddLabel}
          handleCancel={handleCancel}
          isModalOpen={isModalOpen}
          title={`Label`}
          editData={editData}
        />
      </div>
    </div>
  )
}

export default LabelView
