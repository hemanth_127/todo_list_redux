import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  labelData: [],
  loading: false
}

const labelSlice = createSlice({
  name: 'label',
  initialState,
  reducers: {
    setLabelData: (state, action) => {
      state.labelData = action.payload
    },
    setLoading: (state, action) => {
      state.loading = action.payload
    },
    addLabelData: (state, action) => {
      state.labelData.push(action.payload)
    },
    deleteLabelData: (state, action) => {
      state.labelData = state.labelData.filter(ele => ele.id !== action.payload)
    },
    updateLabelData: (state, action) => {
      state.labelData = state.labelData.map(ele => {
        if (ele.id === action.payload.id) {
          return action.payload
        }
        return ele
      })
    }
  }
})

export default labelSlice.reducer
export const {
  setLabelData,
  setLoading,
  deleteLabelData,
  addLabelData,
  updateLabelData
} = labelSlice.actions
