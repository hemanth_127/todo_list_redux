import React, { useState } from 'react'
import { Button, Checkbox, Popover } from 'antd'
import {
  PlusOutlined,
  EditOutlined,
  CalendarOutlined,
  TagOutlined,
  DeleteOutlined,
  EllipsisOutlined
} from '@ant-design/icons'
import FormPage from '../Form/FormPage'
import { useDispatch, useSelector } from 'react-redux'
import {
  addSectionTask,
  completeTask,
  deleteTask,
  updateTask,
  reopenTask
} from '../../Api/api'
import {
  checkboxTaskTodo,
  deleteTaskTodo,
  setTodoData,
  updateTaskTodo,
  reOpenTodo
} from '../TODO/todoSlice'
import { useSnackbar } from 'notistack'
import { setError } from '../Error/errorSlicer'

const SectionTasks = ({ projectId, sectionId }) => {
  const [isModalOpen, setIsModalOpen] = useState(false)
  const { todoData } = useSelector(state => state.todo)
  const [editData, setEditData] = useState(null)

  const dispatch = useDispatch()
  const { enqueueSnackbar } = useSnackbar()

  const showModal = () => {
    setIsModalOpen(true)
  }

  const handleCancel = () => {
    setIsModalOpen(false)
    setEditData(null)
  }

  const handleAddSectionTask = async ({
    name,
    description,
    selectedDate,
    dueString,
    selectedId,
    labelName
  }) => {
    const filterProjectId = selectedId || projectId
    const filterSectionId = selectedId ? '' : sectionId
    try {
      const data = await addSectionTask({
        name,
        projectId: `${filterProjectId}`,
        sectionId: `${filterSectionId}`,
        description,
        selectedDate,
        dueString,
        labelName
      })
      const updateData = [...todoData, data]
      dispatch(setTodoData(updateData))
      setIsModalOpen(false)
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleDeleteTask = async taskId => {
    try {
      const res = await deleteTask(taskId)
      dispatch(deleteTaskTodo(taskId))
      enqueueSnackbar('Task Deleted')
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleChecbox = async taskId => {
    try {
      const res = await completeTask(taskId)
      dispatch(checkboxTaskTodo(taskId))
      enqueueSnackbar('Task Completed', {
        action: <Button onClick={() => handleReOpen(taskId)}>undo</Button>,
        autoHideDuration: 1000
      })
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleReOpen = async id => {
    try {
      const data = await reopenTask(id)
      dispatch(reOpenTodo(id))
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleSetEditData = taskId => {
    const data = todoData.filter(ele => ele.id === taskId)
    const k = data[0]
    setEditData(k)
  }


  const handleUpdateTask = async ({
    name,
    description,
    selectedDate,
    dueString,
    selectedId,
    labelName
  }) => {
    try {
      const res = await updateTask(editData?.id, {
        selectedId,
        name,
        description,
        selectedDate,
        dueString,
        labelName
      })

      dispatch(updateTaskTodo(res))
      setEditData(null)
      setIsModalOpen(false)
      enqueueSnackbar('Task Updated')
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  let todolist = todoData
    .filter(ele => ele.sectionId === sectionId)
    .filter(l => l.isCompleted === false)

  return (
    <div>
      <hr className='row section-row' />
      {todolist &&
        todolist.map(ele => {
          return (
            <div key={ele.id}>
              {editData?.id === ele.id ? (
                <>
                  <FormPage
                    handleAdd={
                      editData ? handleUpdateTask : handleAddSectionTask
                    }
                    handleCancel={handleCancel}
                    isModalOpen={isModalOpen}
                    editData={editData}
                  />
                </>
              ) : (
                <div className='task-item'>
                  <div className='section-task-container'>
                    <div>
                      <Checkbox
                        className='checkbox-item '
                        onChange={() => {
                          handleChecbox(ele.id)
                        }}
                      ></Checkbox>
                      {ele.content}
                    </div>
                    <div className='task-hover'>
                      <Button
                        type='text'
                        onClick={e => {
                          e.stopPropagation()
                          handleSetEditData(ele.id)
                          showModal()
                        }}
                      >
                        <EditOutlined />
                      </Button>

                      <Popover
                        arrow={false}
                        content={
                          <div className='popover-item'>
                            <Button
                              type='text'
                              style={{
                                float: 'right',
                                color: 'red'
                              }}
                              onClick={e => {
                                e.stopPropagation()
                                handleDeleteTask(ele.id)
                              }}
                            >
                              <DeleteOutlined /> Delete
                            </Button>
                          </div>
                        }
                        trigger='click'
                        placement='bottom'
                      >
                        <Button
                          type='text'
                          style={{
                            float: 'right',
                            margin: '0 6px'
                          }}
                          onClick={e => e.stopPropagation()}
                        >
                          <span className='ellipsisicon'>
                            <EllipsisOutlined />
                          </span>
                        </Button>
                      </Popover>
                    </div>
                  </div>
                  <div className='details1'>
                    {ele.description && <p> {ele.description}</p>}
                    <div
                      style={{
                        display: 'flex',
                        paddingTop: '.4rem'
                      }}
                    >
                      <p>
                        {ele.due?.string && (
                          <div className='desription'>
                            <CalendarOutlined />
                            <span>
                              {ele.due.date.slice(-2) +
                                ele.due.string.slice(7, 11)}
                            </span>
                          </div>
                        )}
                      </p>
                      <p>
                        {ele?.labels?.[0] && (
                          <div className='desription'>
                            <TagOutlined />
                            <span>{ele.labels[0]}</span>
                          </div>
                        )}
                      </p>
                    </div>
                  </div>
                  <hr className='row section-row' />
                </div>
              )}
            </div>
          )
        })}
      <div>
        {!isModalOpen && (
          <Button
            type='text'
            className='plus-btn addbtn1'
            icon={<PlusOutlined className='plushover' />}
            onClick={e => {
              e.stopPropagation()
              showModal()
            }}
          >
            <span className='text-color'>Add Task</span>
          </Button>
        )}

        {!editData && (
          <div className='formpage'>
            <FormPage
              handleAdd={editData ? handleUpdateTask : handleAddSectionTask}
              handleCancel={handleCancel}
              isModalOpen={isModalOpen}
              editData={editData}
            />
          </div>
        )}
      </div>
    </div>
  )
}

export default SectionTasks
