import { createSlice } from '@reduxjs/toolkit'
import { updateLabel } from '../../Api/api'

const initialState = {
  sectionData: [],
  loading: false
}

const sectionSlice = createSlice({
  name: 'section',
  initialState,
  reducers: {
    setSectionData: (state, action) => {
      state.sectionData = action.payload
    },
    setLoading: (state, action) => {
      state.loading = action.payload
    },
    addSectionData: (state, action) => {
      state.sectionData.push(action.payload)
    },
    deleteSectionData: (state, action) => {
      state.sectionData = state.sectionData.filter(
        ele => ele.id !== action.payload
      )
    },
    updateSectionData: (state, action) => {
      state.sectionData = state.sectionData.map(ele => {
        if (ele.id === action.payload.id) {
          return action.payload
        } else {
          return ele
        }
      })
    }
  }
})

export default sectionSlice.reducer
export const {
  setSectionData,
  setLoading,
  setSectionTasks,
  deleteSectionData,
  addSectionData,
  updateSectionData
} = sectionSlice.actions
