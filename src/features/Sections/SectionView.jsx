import React, { useEffect, useState } from 'react'
import FormPage from '../Form/FormPage'
import { Button, Popover } from 'antd'
import { useSelector, useDispatch } from 'react-redux'
import {
  setLoading,
  setSectionData,
  addSectionData,
  deleteSectionData,
  updateSectionData
} from './SectionSlice'
import {
  getSections,
  addSection,
  deleteSection,
  updateSection
} from '../../Api/api'
import SectionTasks from './SectionTasks'
import { useSnackbar } from 'notistack'
import { setError } from '../Error/errorSlicer'
import {
  EditOutlined,
  DeleteOutlined,
  EllipsisOutlined
} from '@ant-design/icons'

const SectionView = ({ projectId }) => {
  const [editData, setEditData] = useState(null)

  const [isModalOpen, setIsModalOpen] = useState(false)
  const { sectionData } = useSelector(state => state.section)

  const dispatch = useDispatch()
  const { enqueueSnackbar } = useSnackbar()

  const showModal = () => {
    setIsModalOpen(true)
  }
  const handleCancel = () => {
    setIsModalOpen(false)
  }

  const getSectionData = async () => {
    dispatch(setLoading(true))
    try {
      const data = await getSections(projectId)
      dispatch(setSectionData(data))
    } catch (error) {
      dispatch(setError(error.message))
    } finally {
      dispatch(setLoading(false))
    }
  }

  useEffect(() => {
    getSectionData()
  }, [projectId])

  const handleAddSection = async ({ name }) => {
    try {
      const data = await addSection({ name, projectId })
      dispatch(addSectionData(data))
      setIsModalOpen(false)
      enqueueSnackbar('Added Section')
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleDeleteSection = async sectionId => {
    try {
      const res = await deleteSection(sectionId)
      dispatch(deleteSectionData(sectionId))
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  const handleSetEditData = taskId => {
    const data = sectionData.filter(ele => ele.id === taskId)
    const updateData = data[0]
    setEditData(updateData)
    setIsModalOpen(true)
  }
  const handleEditSection = async ({ name }) => {
    const id = editData.id
    try {
      const data = await updateSection(id, name)
      console.log(data)
      dispatch(updateSectionData(data))
      setIsModalOpen(false)
    } catch (error) {
      dispatch(setError(error.message))
    }
  }

  return (
    <div className='section'>
      {sectionData.map(ele => {
        return (
          <div key={ele.id} className='section-view-container'>
            <div className='section-item'>
              <h4>{ele.name}</h4>

              <Popover
                arrow={false}
                content={
                  <div className='popover-item'>
                    <Button
                      icon={<EditOutlined />}
                      onClick={e => {
                        e.stopPropagation()
                        handleSetEditData(ele.id)
                      }}
                    >
                      Edit
                    </Button>

                    <Button
                      icon={<DeleteOutlined />}
                      onClick={e => {
                        e.stopPropagation()
                        handleDeleteSection(ele.id)
                      }}
                    >
                      Delete
                    </Button>
                  </div>
                }
                trigger='click'
                placement='bottom'
              >
                <Button
                  type='text'
                  style={{
                    float: 'right',
                    margin: '0 6px'
                  }}
                  onClick={e => e.stopPropagation()}
                >
                  <span className='ellipsisicon'>
                    <EllipsisOutlined />
                  </span>
                </Button>
              </Popover>
            </div>

            <div>
              <SectionTasks projectId={projectId} sectionId={ele.id} />
            </div>
          </div>
        )
      })}

      {!isModalOpen && (
        <div
          class='line-container'
          onClick={e => {
            e.stopPropagation()
            showModal()
          }}
        >
          <div class='line'></div>
          <div>&nbsp; Add Section &nbsp;</div>
          <div class='line'></div>
        </div>
      )}

      <div className='formpage'>
        <FormPage
          handleAdd={editData ? handleEditSection : handleAddSection}
          handleCancel={handleCancel}
          isModalOpen={isModalOpen}
          title={`Add section`}
          editData={editData}
        />
      </div>
    </div>
  )
}

export default SectionView
